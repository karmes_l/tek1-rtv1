/*
** my_events.c for  in /home/karmes_l/TP/tp1_minilibx_et_evenements/ex3
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 10 15:18:33 2014 lionel karmes
** Last update Sun Mar 15 13:14:22 2015 lionel karmes
*/

#include "my.h"

int		gere_expose(void *param)
{
  t_param	*mlx;

  mlx = param;
  if (mlx->condition == 0)
    draw_image(mlx->img, mlx->coord, mlx->trigo);
  mlx->condition = 1;
  mlx_put_image_to_window(mlx->img->mlx_ptr, mlx->win_ptr,
			  mlx->img->data_img.img_ptr, 0, 0);
  return (0);
}

int		gere_key(int keycode, void *param)
{
  t_param	*mlx;

  mlx = param;
  if (keycode == K_ECHAP)
    exit(1);
  if (key_move(keycode, mlx) || key_rotate(keycode, mlx))
    {
      mlx->condition = 0;
      gere_expose(param);
    }
  return (0);
}

void		my_events(void *win_ptr, t_win_img *img,
			  t_coord *coord, t_trigo *trigo)
{
  t_param	param;

  param.win_ptr = win_ptr;
  param.img = img;
  param.coord = coord;
  param.trigo = trigo;
  param.condition = 0;
  mlx_key_hook(win_ptr, gere_key, &param);
  mlx_expose_hook(win_ptr, gere_expose, &param);
  mlx_loop(img->mlx_ptr);
}
