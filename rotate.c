/*
** rotate.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1/srcs
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 11 13:59:20 2015 lionel karmes
** Last update Sat Feb 14 14:38:03 2015 lionel karmes
*/

#include "my.h"
#include <stdio.h>

void		rotate_z(t_vect *vect, double alpha)
{
  double	x;
  double	y;
  double	z;

  x = vect->x;
  y = vect->y;
  z = vect->z;
  vect->x = cos(alpha) * x + (-sin(alpha)) * y + 0 * z;
  vect->y = sin(alpha) * x + cos(alpha) * y + 0 * z;
  vect->z = 0 * x + 0 * y + 1 * z;
}

void		rotate_y(t_vect *vect, double alpha)
{
  double	x;
  double	y;
  double	z;

  x = vect->x;
  y = vect->y;
  z = vect->z;
  vect->x = cos(alpha) * x + 0 * y + sin(alpha) * z;
  vect->y = 0 * x + 1 * y + 0 * z;
  vect->z = (-sin(alpha)) * x + 0 * y + cos(alpha) * z;
}

void		rotate_x(t_vect *vect, double alpha)
{
  double	x;
  double	y;
  double	z;

  x = vect->x;
  y = vect->y;
  z = vect->z;
  vect->x = 1 * x + 0 * y + 0 * z;
  vect->y = 0 * x + cos(alpha) * y + (-sin(alpha)) * z;
  vect->z = 0 * x + sin(alpha) * y + cos(alpha) * z;
}
