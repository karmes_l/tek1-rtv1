/*
** draw_image.c for  in /home/karmes_l/TP/Raytracrer_:_GO!
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Feb  3 15:01:49 2015 lionel karmes
** Last update Sun Mar 15 13:12:11 2015 lionel karmes
*/

#include "my.h"

int		calc(t_coord *coord, t_win_img *img, int x, int y)
{
  t_vect	vect;
  int		d;

  d = 100;
  vect.x = d;
  vect.y = img->x / 2 - x;
  vect.z = img->y / 2 - y;
  rotate_x(&vect, coord->position.alpha.x * PI / 180);
  rotate_y(&vect, coord->position.alpha.y * PI / 180);
  rotate_z(&vect, coord->position.alpha.z * PI / 180);
  return (calc_figure(coord, &vect, 0x000000));
}

void	draw_image(t_win_img *img, t_coord *coord, t_trigo *trigo)
{
  int	x;
  int	y;
  int	color;

  (void) trigo;
  y = 0;
  while (y < img->y)
    {
      x = 0;
      while (x < img->x)
  	{
  	  color = calc(coord, img, x, y);
  	  mlx_pixel_put_image(&(img->data_img), x, y, color);
  	  x++;
  	}
      y++;
      my_putnbr(y * 100 / img->y);
      my_putstr("%\r");
    }
}
