/*
** valid_figures.c for  in /home/karmes_l/Projets/Igraph/Raytracer
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb  5 17:02:22 2015 lionel karmes
** Last update Wed Feb 18 09:23:08 2015 lionel karmes
*/

#include "my.h"

int	valid_figure_args_int(char **tab, int ac)
{
  int	i;

  i = 2;
  while (i < ac)
    {
      if (!my_str_isnum(tab[i]))
	{
	  my_putstrerror("[ERROR] : the 3 to ");
	  my_putnbr(ac);
	  my_putstrerror(" arguments must be integers");
	  return (0);
	}
      i++;
    }
  return (1);
}

int	valid_figure(char **tab, int ac)
{
  int	(*ptr[5])(char **, int);
  int	i;

  ptr[0] = &valid_figure_droite;
  ptr[1] = &valid_figure_ellipse;
  ptr[2] = &valid_figure_plan;
  ptr[3] = &valid_figure_cylindre;
  ptr[4] = &valid_figure_cone;
  i = what_figure_next(tab[0]);
  if (i == -1)
    {
      my_putstrerror("[ERROR] : invalid type of figure");
      return (0);
    }
  return (ptr[i](tab, ac));
}

int	valid_light(char **tab, int ac)
{
  if (!my_strcmp("light", tab[0]))
    {
      if (ac != 6)
	{
	  my_putstrerror("[ERROR] : type comment brightness x y z\n");
	  return (0);
	}
      return (valid_figure_args_int(tab, ac));
    }
  my_putstrerror("[ERROR] : invalid type of lights");
  return (0);
}

int	valid_list(char **tab, int ac, int what_list)
{
  if (what_list == FIGURES)
    return (valid_figure(tab, ac));
  else if (what_list == LIGHTS)
    return (valid_light(tab, ac));
  else
    my_putstrerror("[ERROR] : inconnue (what_list ?)\n");
  return (-1);
}
