/*
** calc_figure.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Feb  6 12:42:05 2015 lionel karmes
** Last update Sun Mar 15 14:23:18 2015 lionel karmes
*/

#include "my.h"

t_inter_tmp	calc_cone(t_position *eyes, t_vect *vect, void *figure)
{
  t_inter	inter;
  t_cone	*cone;
  t_coord_tmp	coord_tmp;
  t_inter_tmp	inter_tmp;

  cone = figure;
  init_coord_tmp(&coord_tmp, eyes->point, cone->point, vect);
  rotate_xyz(&(coord_tmp.vect), cone->alpha);
  rotate_xyz(&(coord_tmp.eyes), cone->alpha);
  inter.a = pow(coord_tmp.vect.x, 2) + pow(coord_tmp.vect.y, 2)
    - pow(coord_tmp.vect.z, 2) * pow(tan(cone->angle * PI / 180), 2);
  inter.b = 2 * (coord_tmp.vect.x * coord_tmp.eyes.x
		 + coord_tmp.vect.y * coord_tmp.eyes.y
		 - coord_tmp.vect.z * coord_tmp.eyes.z
		 * pow(tan(cone->angle * PI / 180), 2));
  inter.c = pow(coord_tmp.eyes.x, 2) + pow(coord_tmp.eyes.y, 2)
    - pow(coord_tmp.eyes.z, 2) * pow(tan(cone->angle * PI / 180), 2);
  inter.delta = pow(inter.b, 2) - 4 * inter.a * inter.c;
  inter_tmp.k = intersection(&inter);
  inter_tmp.tmp_eyes = coord_tmp.eyes;
  return (inter_tmp);
}

t_inter_tmp	calc_cylindre(t_position *eyes, t_vect *vect, void *figure)
{
  t_inter	inter;
  t_cylindre	*cylindre;
  t_coord_tmp	coord_tmp;
  t_inter_tmp	inter_tmp;

  cylindre = figure;
  init_coord_tmp(&coord_tmp, eyes->point, cylindre->point, vect);
  rotate_xyz(&(coord_tmp.vect), cylindre->alpha);
  rotate_xyz(&(coord_tmp.eyes), cylindre->alpha);
  inter.a = pow(coord_tmp.vect.x, 2) + pow(coord_tmp.vect.y, 2);
  inter.b = 2 * (coord_tmp.vect.x * coord_tmp.eyes.x
		 + coord_tmp.vect.y * coord_tmp.eyes.y);
  inter.c = pow(coord_tmp.eyes.x, 2) + pow(coord_tmp.eyes.y, 2)
    - pow(cylindre->rayon, 2);
  inter.delta = pow(inter.b, 2) - 4 * inter.a * inter.c;
  inter_tmp.k = intersection(&inter);
  inter_tmp.tmp_eyes = coord_tmp.eyes;
  return (inter_tmp);
}

t_inter_tmp	calc_plan(t_position *eyes, t_vect *vect, void *figure)
{
  t_plan	*plan;
  float		k;
  t_coord_tmp	coord_tmp;
  t_inter_tmp	inter_tmp;

  plan = figure;
  init_coord_tmp(&coord_tmp, eyes->point, plan->point, vect);
  rotate_xyz(&(coord_tmp.vect), plan->alpha);
  rotate_xyz(&(coord_tmp.eyes), plan->alpha);
  if (ABS(coord_tmp.vect.z) < 0.00001 && coord_tmp.eyes.z != 0)
    {
      inter_tmp.k = -1;
      return (inter_tmp);
    }
  else if (ABS(coord_tmp.vect.z) < 1 && coord_tmp.eyes.z == 0)
    {
      inter_tmp.k = 1;
      return (inter_tmp);
    }
  k = -(coord_tmp.eyes.z / coord_tmp.vect.z);
  inter_tmp.k = k;
  inter_tmp.tmp_eyes = coord_tmp.eyes;
  return (inter_tmp);
}

t_inter_tmp	calc_droite(t_position *eyes, t_vect *vect, void *figure)
{
  t_droite	*droite;
  t_inter_tmp	inter_tmp;

  droite = figure;
  (void) eyes;
  (void) vect;
  (void) droite;
  inter_tmp.k = -1;
  return (inter_tmp);
}

t_inter_tmp	calc_ellipse(t_position *eyes, t_vect *vect, void *figure)
{
  t_inter	inter;
  t_ellipse	*ellipse;
  t_coord_tmp	coord_tmp;
  t_inter_tmp	inter_tmp;

  ellipse = figure;
  init_coord_tmp(&coord_tmp, eyes->point, ellipse->point, vect);
  rotate_xyz(&(coord_tmp.vect), ellipse->alpha);
  rotate_xyz(&(coord_tmp.eyes), ellipse->alpha);
  inter.a = pow(coord_tmp.vect.x, 2) + pow(coord_tmp.vect.y, 2)
    + pow(coord_tmp.vect.z, 2);
  inter.b = 2 * (coord_tmp.eyes.x * coord_tmp.vect.x + coord_tmp.eyes.y
		 * coord_tmp.vect.y + coord_tmp.eyes.z * coord_tmp.vect.z);
  inter.c = pow(coord_tmp.eyes.x, 2) + pow(coord_tmp.eyes.y, 2)
    + pow(coord_tmp.eyes.z, 2) - pow(ellipse->rayon1, 2);
  inter.delta = pow(inter.b, 2) - 4 * inter.a * inter.c;
  inter_tmp.k = intersection(&inter);
  inter_tmp.tmp_eyes = coord_tmp.eyes;
  return (inter_tmp);
}
