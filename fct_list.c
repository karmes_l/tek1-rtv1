/*
** init_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:41:33 2014 lionel karmes
** Last update Sun Mar 15 13:10:26 2015 lionel karmes
*/

#include "my.h"

void		my_put_in_list_end(t_list **list, char **tab, int id,
				   int what_list)
{
  t_element	*element;

  if ((element = malloc(sizeof(t_element))) == NULL)
    exit(0);
  element->id = id;
  if ((element->type = my_strdup(tab[0])) == NULL)
    exit(1);
  if ((element->comment = my_strdup(tab[1])) == NULL)
    exit(1);
  put_in_list(element, tab, what_list);
  element->e_next = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_prev = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      (*list)->l_end->e_next = element;
      element->e_prev = (*list)->l_end;
      (*list)->l_end = element;
    }
  (*list)->size++;
}

void		my_put_in_list_start(t_list **list, char **tab, int id,
				     int what_list)
{
  t_element	*element;

  if ((element = malloc(sizeof(t_element))) == NULL)
    exit(0);
  element->id = id;
  if ((element->type = my_strdup(tab[0])) == NULL)
    exit(1);
  if ((element->comment = my_strdup(tab[1])) == NULL)
    exit(1);
  put_in_list(element, tab, what_list);
  element->e_prev = NULL;
  if ((*list)->l_start == NULL)
    {
      element->e_next = NULL;
      (*list)->l_end = element;
      (*list)->l_start = element;
    }
  else
    {
      (*list)->l_start->e_prev = element;
      element->e_next = (*list)->l_start;
      (*list)->l_start = element;
    }
  (*list)->size++;
}

t_list		*new_list()
{
  t_list	*list;

  if ((list = malloc(sizeof(t_list))) == NULL)
    exit(0);
  list->size = 0;
  list->l_start = NULL;
  list->l_end = NULL;
  return (list);
}

void		remove_list(t_list **list)
{
  t_element	*tmp;
  t_element	*element;

  if (*list != NULL)
    {
      tmp = (*list)->l_start;
      	while (tmp != NULL)
      	  {
      	    element = tmp;
	    free(tmp->comment);
	    free(tmp->type);
	    free(tmp->figure);
      	    tmp = tmp->e_next;
      	    free(element);
      	  }
      free(*list);
      *list = NULL;
    }
}
