/*
** add_figure.c for  in /home/karmes_l/Projets/Igraph/Raytracer
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb  5 18:41:06 2015 lionel karmes
** Last update Sun Mar 15 13:12:44 2015 lionel karmes
*/

#include "my.h"

void		add_figure_cylindre(t_element *element, char **tab)
{
  t_cylindre	*cylindre;

  if ((cylindre = pmalloc(sizeof(t_cylindre))) == NULL)
	exit(1);
  cylindre->rayon = my_getnbr(tab[2]);
  cylindre->point.x = my_getnbr(tab[3]);
  cylindre->point.y = my_getnbr(tab[4]);
  cylindre->point.z = my_getnbr(tab[5]);
  cylindre->alpha.x = my_getnbr(tab[6]);
  cylindre->alpha.y = my_getnbr(tab[7]);
  cylindre->alpha.z = my_getnbr(tab[8]);
  cylindre->color = my_getnbr(tab[9]);
  element->figure = cylindre;
}

void		add_figure_cone(t_element *element, char **tab)
{
  t_cone	*cone;

  if ((cone = pmalloc(sizeof(t_cone))) == NULL)
	exit(1);
  cone->angle = my_getnbr(tab[2]);
  cone->point.x = my_getnbr(tab[3]);
  cone->point.y = my_getnbr(tab[4]);
  cone->point.z = my_getnbr(tab[5]);
  cone->alpha.x = my_getnbr(tab[6]);
  cone->alpha.y = my_getnbr(tab[7]);
  cone->alpha.z = my_getnbr(tab[8]);
  cone->color = my_getnbr(tab[9]);
  element->figure = cone;
}

void		add_figure_droite(t_element *element, char **tab)
{
  t_droite	*droite;

  if ((droite = pmalloc(sizeof(t_droite))) == NULL)
    exit(1);
  droite->point1.x = my_getnbr(tab[2]);
  droite->point1.y = my_getnbr(tab[3]);
  droite->point1.z = my_getnbr(tab[4]);
  droite->point2.x = my_getnbr(tab[5]);
  droite->point2.y = my_getnbr(tab[6]);
  droite->point2.z = my_getnbr(tab[7]);
  droite->point1.color = my_getnbr(tab[8]);
  droite->point2.color = my_getnbr(tab[8]);
  element->figure = droite;
}

void		add_figure_ellipse(t_element *element, char **tab)
{
  t_ellipse	*ellipse;

  if ((ellipse = pmalloc(sizeof(t_ellipse))) == NULL)
    exit(1);
  ellipse->rayon1 = my_getnbr(tab[2]);
  ellipse->rayon2 = my_getnbr(tab[3]);
  ellipse->point.x = my_getnbr(tab[4]);
  ellipse->point.y = my_getnbr(tab[5]);
  ellipse->point.z = my_getnbr(tab[6]);
  ellipse->alpha.x = my_getnbr(tab[7]);
  ellipse->alpha.y = my_getnbr(tab[8]);
  ellipse->alpha.z = my_getnbr(tab[9]);
  ellipse->color = my_getnbr(tab[10]);
  element->figure = ellipse;
}

void		add_figure_plan(t_element *element, char **tab)
{
  t_plan	*plan;

  if ((plan = pmalloc(sizeof(t_plan))) == NULL)
    exit(1);
  plan->point.x = my_getnbr(tab[2]);
  plan->point.y = my_getnbr(tab[3]);
  plan->point.z = my_getnbr(tab[4]);
  plan->alpha.x = my_getnbr(tab[5]);
  plan->alpha.y = my_getnbr(tab[6]);
  plan->alpha.z = my_getnbr(tab[7]);
  plan->color = my_getnbr(tab[8]);
  element->figure = plan;
}
