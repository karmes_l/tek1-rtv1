/*
** my_wolf3D.c for  in /home/karmes_l/TP/Wolf3D
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Dec 15 10:53:11 2014 lionel karmes
** Last update Sun Mar 15 15:05:33 2015 lionel karmes
*/

#include "my.h"

void		init_all(void *mlx_ptr, t_win_img *img, t_coord *coord,
			 t_trigo *trigo)
{
  t_list	*list;
  t_list	*lights;
  t_position	position;

  img->mlx_ptr = mlx_ptr;
  img->x = SIZE_WIN_X;
  img->y = SIZE_WIN_Y;
  position.point.x = -300;
  position.point.y = 0;
  position.point.z = 100;
  position.alpha.x = 0;
  position.alpha.y = 0;
  position.alpha.z = 0;
  coord->position = position;
  list = init_list("figures_data", FIGURES);
  lights = init_list("lights_data", LIGHTS);
  coord->figures = list;
  coord->lights = lights;
  init_trigo(trigo);
}

void		raytracer()
{
  void		*mlx_ptr;
  void		*win_ptr;
  t_win_img	img;
  t_coord	coord;
  t_trigo	trigo;

  if ((mlx_ptr = mlx_init()) == NULL)
    {
      my_putstrerror("[ERROR] : mlx_init()\n");
      exit(1);
    }
  if ((win_ptr = mlx_new_window(mlx_ptr, SIZE_WIN_X, SIZE_WIN_Y, "Wolf3D"))
      == NULL)
    {
      my_putstrerror("[ERROR] : mlx_new_window()\n");
      exit(1);
    }
  init_all(mlx_ptr, &img, &coord, &trigo);
  image(&img);
  my_events(win_ptr, &img, &coord, &trigo);
}
