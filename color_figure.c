/*
** calc_figure.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Feb  6 12:42:05 2015 lionel karmes
** Last update Sun Mar 15 13:07:11 2015 lionel karmes
*/

#include "my.h"
#include <stdio.h>

int	color_cone(void *figure, t_vect pt_inter, t_list *lights)
{
  t_cone	*cone;
  t_vect	normale;

  cone = figure;
  normale.x = pt_inter.x;
  normale.y = pt_inter.y;
  normale.z = -tan(cone->angle * PI / 180) * pt_inter.z;
  rotate_xyz(&normale, cone->alpha);
  return (calc_light(lights, &normale, pt_inter, cone->color));
}

int	color_cylindre(void *figure, t_vect pt_inter, t_list *lights)
{
  t_cylindre	*cylindre;
  t_vect	normale;

  cylindre = figure;
  normale.x = pt_inter.x;
  normale.y = pt_inter.y;
  normale.z = 0;
  rotate_xyz(&normale, cylindre->alpha);
  return (calc_light(lights, &normale, pt_inter, cylindre->color));
}

int	color_plan(void *figure, t_vect pt_inter, t_list *lights)
{
  t_plan	*plan;
  t_vect	normale;

  plan = figure;
  (void) pt_inter;
  normale.x = 0;
  normale.y = 0;
  normale.z = 100;
  rotate_xyz(&normale, plan->alpha);
  return (calc_light(lights, &normale, pt_inter, plan->color));
}

int	color_ellipse(void *figure, t_vect pt_inter, t_list *lights)
{
  t_ellipse	*ellipse;
  t_vect	normale;

  ellipse = figure;
  normale.x = pt_inter.x;
  normale.y = pt_inter.y;
  normale.z = pt_inter.z;
  rotate_xyz(&normale, ellipse->alpha);
  return (calc_light(lights, &normale, pt_inter, ellipse->color));
}

int	color_droite(void *figure, t_vect pt_inter, t_list *lights)
{
  t_droite	*droite;

  (void) pt_inter;
  (void) lights;
  droite = figure;
  return (droite->point1.color);
}
