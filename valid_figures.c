/*
** valid_figures.c for  in /home/karmes_l/Projets/Igraph/Raytracer
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb  5 17:02:22 2015 lionel karmes
** Last update Wed Feb 18 09:06:23 2015 lionel karmes
*/

#include "my.h"

int	valid_figure_cylindre(char **tab, int ac)
{
  if (ac != 10)
    {
      my_putstrerror("[ERROR] : type comment rayon x y z");
      my_putstrerror(" xalpha yalpha zalpha color");
      return (0);
    }
  return (valid_figure_args_int(tab, ac));
}

int	valid_figure_cone(char **tab, int ac)
{
  if (ac != 10)
    {
      my_putstrerror("[ERROR] : type comment angle x y z");
      my_putstrerror(" xalpha yalpha zalpha color");
      return (0);
    }
  return (valid_figure_args_int(tab, ac));
}

int	valid_figure_droite(char **tab, int ac)
{
  if (ac != 12)
    {
      my_putstrerror("[ERROR] : type comment x1 y1 z1 x2 y2 z2");
      my_putstrerror(" xalpha yalpha zalpha color");
      return (0);
    }
  return (valid_figure_args_int(tab, ac));
}

int	valid_figure_ellipse(char **tab, int ac)
{
  if (ac != 11)
    {
      my_putstrerror("[ERROR] : type comment rayon1 rayon2 x y z");
      my_putstrerror(" xalpha yalpha zalpha color");
      return (0);
    }
  return (valid_figure_args_int(tab, ac));
}

int	valid_figure_plan(char **tab, int ac)
{
  if (ac != 9)
    {
      my_putstrerror("[ERROR] : type comment x y z");
      my_putstrerror(" xalpha yalpha zalpha color");
      return (0);
    }
  return (valid_figure_args_int(tab, ac));
}
