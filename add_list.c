/*
** add_figure.c for  in /home/karmes_l/Projets/Igraph/Raytracer
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Feb  5 18:41:06 2015 lionel karmes
** Last update Wed Feb 18 16:10:07 2015 lionel karmes
*/

#include "my.h"

void	put_in_list_figure(t_element *element, char **tab)
{
  void	(*ptr[5])(t_element *, char **);
  int	i;

  i = 0;
  ptr[0] = &add_figure_droite;
  ptr[1] = &add_figure_ellipse;
  ptr[2] = &add_figure_plan;
  ptr[3] = &add_figure_cylindre;
  ptr[4] = &add_figure_cone;
  i = what_figure(tab[0]);
  ptr[i](element, tab);
}

void	put_in_list_lights(t_element *element, char **tab)
{
  t_light	*light;

  if ((light = pmalloc(sizeof(t_light))) == NULL)
    exit(1);
  light->bright = my_getnbr(tab[2]);
  light->point.x = my_getnbr(tab[3]);
  light->point.y = my_getnbr(tab[4]);
  light->point.z = my_getnbr(tab[5]);
  element->figure = light;
}

void	put_in_list(t_element *element, char **tab, int what_list)
{
  if (what_list == FIGURES)
    put_in_list_figure(element, tab);
  else if (what_list == LIGHTS)
    put_in_list_lights(element, tab);
  else
    my_putstrerror("[ERROR] : MEGA inconnue\n");
}
