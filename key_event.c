/*
** key_event.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 11 15:24:39 2015 lionel karmes
** Last update Sun Feb 22 21:36:52 2015 lionel karmes
*/

#include "my.h"

int	key_move(int keycode, t_param *param)
{
  int	move;

  move = 10;
  if (keycode == K_UP)
    param->coord->position.point.x += move;
  else if (keycode == K_DOWN)
    param->coord->position.point.x -= move;
  else if (keycode == K_LEFT)
    param->coord->position.point.y += move;
  else if (keycode == K_RIGHT)
    param->coord->position.point.y -= move;
  else if (keycode == K_UP2)
    param->coord->position.point.z += move;
  else if (keycode == K_DOWN2)
    param->coord->position.point.z -= move;
  else
    return (0);
  return (1);
}

int		key_rotate(int keycode, t_param *param)
{
  double	alpha;

  alpha = 5;
  if (keycode == K_X)
    param->coord->position.alpha.x += alpha;
  else if (keycode == K_Y)
    param->coord->position.alpha.y += alpha;
  else if (keycode == K_Z)
    param->coord->position.alpha.z += alpha;
  else if (keycode == K_C)
    param->coord->position.alpha.x -= alpha;
  else if (keycode == K_U)
    param->coord->position.alpha.y -= alpha;
  else if (keycode == K_E)
    param->coord->position.alpha.z -= alpha;
  else
    return (0);
  return (1);
}
