/*
** init_list.c for  in /home/karmes_l/Projets/Prog_Elem/PushSwap/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Tue Dec  9 14:41:33 2014 lionel karmes
** Last update Sun Mar 15 13:11:36 2015 lionel karmes
*/

#include "my.h"

int	open_file(char *path)
{
  int	fd;

  if ((fd = open(path, O_RDONLY)) < 0)
    {
      my_putstrerror("[ERROR] : open file : ");
      my_putstrerror(path);
      my_putcharerror('\n');
      exit(2);
    }
  return (fd);
}

int	init_list_next(t_list *list, char **tab, int i, int what_list)
{
  int	ac;
  int	b;

  ac = my_strtablen(tab);
  if (ac > 0)
    {
      b = valid_list(tab, ac, what_list);
      if (b)
	my_put_in_list_start(&list, tab, i, what_list);
      return (b);
    }
  return (1);
}

void		print_list(t_list *list)
{
  t_element	*tmp;
  t_light	*light;

  tmp = list->l_start;
  while (tmp != NULL)
    {
      light = tmp->figure;
      my_putnbr(light->point.x);
      tmp = tmp->e_next;
    }
}

void		message_error_list(char *path, int line)
{
  my_putstrerror(", error : load file ");
  my_putstrerror(path);
  my_putstrerror(" line ");
  my_putnbr(line + 1);
  my_putcharerror('\n');
}

t_list		*init_list(char *path, int what_list)
{
  t_list	*list;
  char		*s;
  int		fd;
  char		**s_sorted;
  int		i;

  list = new_list();
  i = 0;
  fd = open_file(path);
  while ((s = get_next_line(fd)) != NULL)
    {
      s_sorted = my_str_to_wordtab(s, ' ');
      if (init_list_next(list, s_sorted, i, what_list) == 0)
	message_error_list(path, i);
      free(s);
      free_tab(s_sorted);
      i++;
    }
  if (fd >= 0)
    close(fd);
  return (list);
}
