/*
** calc_light.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1/srcs
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 18 12:05:00 2015 lionel karmes
** Last update Sun Mar 15 15:01:15 2015 lionel karmes
*/

#include "my.h"

double		norme(t_vect *vect)
{
  return (sqrt(pow(vect->x, 2) + pow(vect->y, 2) + pow(vect->z, 2)));
}

double		calc_light1(void *figure, t_vect *normale, t_vect pt_inter)
{
  t_light	*light;
  double	cos_alpha;
  t_vect	vect_l;
  t_vect	vect_tmp;

  light = figure;
  vect_tmp.x = light->point.x;
  vect_tmp.y = light->point.y;
  vect_tmp.z = light->point.z;
  translation(&vect_l, &pt_inter, &vect_tmp);
  cos_alpha = (normale->x * vect_l.x + normale->y * vect_l.y
	       + normale->z * vect_l.z)
    / (norme(normale) * norme(&vect_l));
  return (cos_alpha);
}

int		calc_light(t_list *lights, t_vect *normale,
			   t_vect pt_inter, int color)
{
  t_element	*tmp;
  double	cos_alpha;

  tmp = lights->l_start;
  cos_alpha = 0;
  while (tmp != NULL)
    {
      cos_alpha += calc_light1(tmp->figure, normale, pt_inter);
      tmp = tmp->e_next;
    }
  cos_alpha /= lights->size;
  if (cos_alpha <= 0)
    color = 0x000000;
  else
    {
      color = ((int) (((color & 0xFF0000) >> 16) * cos_alpha) << 16)
	+ ((int) (((color & 0x00FF00) >> 8) * cos_alpha) << 8)
	+ (color & 0x0000FF) * cos_alpha;
    }
  return (color);
}
