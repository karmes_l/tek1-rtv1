/*
** calc_figure.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Feb  6 12:42:05 2015 lionel karmes
** Last update Sun Mar 15 13:06:39 2015 lionel karmes
*/

#include "my.h"

int	what_figure_next(char *str)
{
  return (((!my_strcmp("droite", str)) ? DROITE :
	   ((!my_strcmp("ellipse", str)) ? ELLIPSE :
	    ((!my_strcmp("plan", str)) ? PLAN :
	     ((!my_strcmp("cylindre", str)) ? CYLINDRE :
	      ((!my_strcmp("cone", str)) ? CONE :
	       -1))))));
}

int	what_figure(char *str)
{
  int	i;

  i = what_figure_next(str);
  if (i == -1)
    {
      my_putstrerror("[ERROR] : inconnue\n");
      exit(0);
    }
  return (i);
}
