/*
** calc_figure.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Feb  6 12:42:05 2015 lionel karmes
** Last update Sun Mar 15 13:11:23 2015 lionel karmes
*/

#include "my.h"

void	init_calc_ptr(t_inter_tmp (*ptr_calc[])
		      (t_position *, t_vect *, void *),
		      int (*ptr_color[])(void *, t_vect, t_list *))
{
  ptr_calc[0] = &calc_droite;
  ptr_calc[1] = &calc_ellipse;
  ptr_calc[2] = &calc_plan;
  ptr_calc[3] = &calc_cylindre;
  ptr_calc[4] = &calc_cone;
  ptr_color[0] = &color_droite;
  ptr_color[1] = &color_ellipse;
  ptr_color[2] = &color_plan;
  ptr_color[3] = &color_cylindre;
  ptr_color[4] = &color_cone;
}

void	rotate_xyz(t_vect *vect, t_vect alpha)
{
  rotate_x(vect, (alpha.x * PI / 180));
  rotate_y(vect, (alpha.y * PI / 180));
  rotate_z(vect, (alpha.z * PI / 180));
}

void	translation(t_vect *trans, t_vect *vect1, t_vect *vect2)
{
  trans->x = vect2->x - vect1->x;
  trans->y = vect2->y - vect1->y;
  trans->z = vect2->z - vect1->z;
}

void	init_coord_tmp(t_coord_tmp *coord_tmp, t_vect eyes,
		       t_vect point, t_vect *vect)
{
  translation(&(coord_tmp->eyes), &point, &eyes);
  coord_tmp->vect.x = vect->x;
  coord_tmp->vect.y = vect->y;
  coord_tmp->vect.z = vect->z;
}
