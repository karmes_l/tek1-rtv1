/*
** calc_figure.c for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Fri Feb  6 12:42:05 2015 lionel karmes
** Last update Sun Mar 15 15:01:39 2015 lionel karmes
*/

#include "my.h"

double	intersection(t_inter *inter)
{
  if (inter->delta >= 0)
    {
      if ((inter->k = MIN((-inter->b - sqrt(inter->delta)) / (2 * inter->a),
		   (-inter->b + sqrt(inter->delta)) / (2 * inter->a))) > 0)
	return (inter->k);
      else
	return (MAX((-inter->b - sqrt(inter->delta)) / (2 * inter->a),
		    (-inter->b + sqrt(inter->delta)) / (2 * inter->a)));
    }
  else
    return (-1);
}

t_vect		pt_inter(t_inter_tmp *inter_eyes, t_vect *vect)
{
  t_vect	p_inter;

  p_inter.x = inter_eyes->tmp_eyes.x + inter_eyes->k * vect->x;
  p_inter.y = inter_eyes->tmp_eyes.y + inter_eyes->k * vect->y;
  p_inter.z = inter_eyes->tmp_eyes.z + inter_eyes->k * vect->z;
  return (p_inter);
}

int		calc_figure(t_coord *coord, t_vect *vect, int color)
{
  t_element	*tmp;
  t_inter_tmp	(*ptr_calc[5])(t_position *, t_vect *, void *);
  int		(*ptr_color[5])(void *, t_vect, t_list *);
  int		i;
  t_inter_tmp	inter_eyes;
  t_inter_tmp	inter_eyes_tmp;

  inter_eyes.k = -1;
  init_calc_ptr(ptr_calc, ptr_color);
  tmp = coord->figures->l_start;
  while (tmp != NULL)
    {
      i = what_figure(tmp->type);
      inter_eyes_tmp = ptr_calc[i](&(coord->position), vect, tmp->figure);
      if ((inter_eyes_tmp.k < inter_eyes.k || inter_eyes.k == -1)
	  && inter_eyes_tmp.k > 0)
	{
	  inter_eyes = inter_eyes_tmp;
	  color = ptr_color[i](tmp->figure, pt_inter(&inter_eyes, vect),
	  		       coord->lights);
	}
      tmp = tmp->e_next;
    }
  return (color);
}
