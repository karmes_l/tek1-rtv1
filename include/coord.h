/*
** coord.h for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec  3 10:14:39 2014 lionel karmes
** Last update Mon Feb 23 15:12:14 2015 lionel karmes
*/

#ifndef COORD_H_
# define COORD_H_

# include "list.h"

typedef struct	s_position
{
  t_vect	point;
  t_vect	alpha;
}		t_position;

typedef struct	s_coord
{
  /* A remplir d'autres coord */
  t_list	*figures;
  t_list	*lights;
  t_position	position;
}		t_coord;

typedef struct	s_coord_tmp
{
  t_vect	eyes;
  t_vect	vect;
}		t_coord_tmp;

typedef struct	s_inter_tmp
{
  t_vect	tmp_eyes;
  double	k;
}		t_inter_tmp;

#endif /* !COORD_H_ */
