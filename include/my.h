/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Sun Mar 15 14:57:24 2015 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include <math.h>
# include <errno.h>
# include <string.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include "mlx.h"
# include "get_next_line.h"
# include "coord.h"
# include "figure.h"
# include "trigo.h"
# include "pixel_image.h"
# include "events.h"
# include "list.h"
# include "light.h"

char		*convert_base(char *, char *, char *);
int		count_num(long);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putcharerror(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
void		my_putstrerror(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*mu_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_strdup(char *);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
char		**my_strnum_to_wordtab(char *);
char		**my_str_to_wordtab(char *, char);
int		my_strtablen(char **);
void		free_tab(char **);
void		*pmalloc(int);
void		raytracer();
void		my_events(void *, t_win_img *, t_coord *, t_trigo *);
void		image(t_win_img *);
void		draw_image(t_win_img *, t_coord *, t_trigo *);
t_data_img	init_data_img(void *, char *, int, int);
void		mlx_pixel_put_image(t_data_img *, int, int, int);
void		droite_img(t_data_img *, t_droite *, int);
int		droite1_img(t_data_img *, t_droite *, int);
int		droite2_img(t_data_img *, t_droite *, int);
int		droite3_img(t_data_img *, t_droite *, int);
void		rectangle_img(t_data_img *, t_droite *, int);
void		ellipse_img(t_data_img *, t_ellipse *, int, t_trigo *);
void		init_trigo(t_trigo *);
int		calc(t_coord *, t_win_img *, int, int);
int		calc_figure(t_coord *, t_vect *, int);
t_inter_tmp	calc_plan(t_position *, t_vect *, void *);
t_inter_tmp	calc_ellipse(t_position *, t_vect *, void *);
t_inter_tmp	calc_droite(t_position *, t_vect *, void *);
t_inter_tmp	calc_cylindre(t_position *, t_vect *, void *);
t_inter_tmp	calc_cone(t_position *, t_vect *, void *);
t_list		*init_list(char *, int);
void		print_list(t_list *);
int		what_figure(char *);
int		what_figure_next(char *);
int		valid_list(char **, int, int);
int		valid_figure_args_int(char **, int);
int		valid_figure_droite(char **, int);
int		valid_figure_ellipse(char **, int);
int		valid_figure_plan(char **, int);
int		valid_figure_cylindre(char **, int);
int		valid_figure_cone(char **, int);
void		my_put_in_list_start(t_list **, char **, int, int);
void		my_put_in_list_end(t_list **, char **, int, int);
void		put_in_list(t_element *, char **, int);
void		add_figure_droite(t_element *, char **);
void		add_figure_ellipse(t_element *, char **);
void		add_figure_plan(t_element *, char **);
void		add_figure_cylindre(t_element *, char **);
void		add_figure_cone(t_element *, char **);
t_list		*new_list();
void		remove_list(t_list **);
void		init_calc_ptr(t_inter_tmp (*[])(t_position *, t_vect *, void *),
			      int (*[])(void *, t_vect, t_list *));
double		intersection(t_inter *);
int		color_droite(void *, t_vect, t_list *);
int		color_ellipse(void *, t_vect, t_list *);
int		color_plan(void *, t_vect, t_list *);
int		color_cylindre(void *, t_vect, t_list *);
int		color_cone(void *, t_vect, t_list *);
void		rotate_z(t_vect *, double);
void		rotate_y(t_vect *, double);
void		rotate_x(t_vect *, double);
int		key_rotate(int, t_param *);
int		key_move(int, t_param *);
void		rotate_xyz(t_vect *, t_vect);
void		rotate_xyz_inv(t_vect *, t_vect);
void		init_coord_tmp(t_coord_tmp *, t_vect, t_vect, t_vect *);
int		calc_light(t_list *, t_vect *, t_vect, int);
void		translation(t_vect *, t_vect *, t_vect *);

# define SIZE_WIN_X (600)
# define SIZE_WIN_Y (400)

#endif /* !MY_H_ */
