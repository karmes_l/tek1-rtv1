/*
** light.h for  in /home/karmes_l/Projets/Igraph/Raytracer/v1/rtv1/srcs
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Feb 18 09:24:22 2015 lionel karmes
** Last update Wed Feb 18 15:45:41 2015 lionel karmes
*/

#ifndef LIGHT_H_
# define LIGHT_H_

#include "figure.h"

typedef struct	s_light
{
  t_vect	point;
  int		bright;
}		t_light;

#endif /* !LIGHT_H_ */
