/*
** droite.h for  in /home/karmes_l/Projets/FDF/test/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 17 12:07:10 2014 lionel karmes
** Last update Sun Feb 22 21:20:26 2015 lionel karmes
*/

#ifndef FIGURE_H_
# define FIGURE_H_

# define PI		(3.14159265359)
# define MAX(v1, v2)	((v1) > (v2) ? (v1) : (v2))
# define MIN(v1, v2)	((v1) < (v2) ? (v1) : (v2))
# define ABS(v1)	((v1) < 0 ? (-(v1)) : (v1))

typedef struct	s_inter
{
  double	a;
  double	b;
  double	c;
  double	delta;
  double	k;
}		t_inter;

typedef struct	s_pixel
{
  int		x;
  int		y;
  int		z;
  int		color;
}		t_pixel;

typedef struct	s_vect
{
  double	x;
  double	y;
  double	z;
}		t_vect;

typedef	struct	s_droite
{
  t_pixel	point1;
  t_pixel	point2;
}		t_droite;

typedef struct	s_ellipse
{
  int		rayon1;
  int		rayon2;
  t_vect	point;
  int		color;
  t_vect	alpha;
}		t_ellipse;

typedef struct	s_plan
{
  t_vect	point;
  int		color;
  t_vect	alpha;
}		t_plan;

typedef struct	s_cylindre
{
  int		rayon;
  t_vect	point;
  int		color;
  t_vect	alpha;
}		t_cylindre;

typedef struct	s_cone
{
  double	angle;
  t_vect	point;
  int		color;
  t_vect	alpha;
}		t_cone;

# define DROITE (0)
# define ELLIPSE (1)
# define PLAN (2)
# define CYLINDRE (3)
# define CONE (4)

#endif /* !FIGURE_H_ */
